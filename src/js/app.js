"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import Dropdown from "./components/dropdown.js";
import SimpleBar from 'simplebar'; // Кастомный скролл
import {DataTable} from "simple-datatables"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Dropdown
Dropdown();


const filter = () => {
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-toggle]')) {
            event.target.closest('[data-filter]').classList.toggle('open')
        }
    })
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-clear]')) {
            event.target.closest('[data-filter-content]').querySelectorAll('input').forEach(el => {
                el.checked = false
            })
        }
    })
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-filter-all]')) {
            event.target.closest('[data-filter-content]').querySelectorAll('input').forEach(el => {
                el.checked = true
            })
        }
    })
    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-reset-all]')) {
            event.target.closest('.section').querySelectorAll('.checkbox__input').forEach(el => {
                el.checked = false
            })
        }
    })

}

filter()

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
    document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
        new SimpleBar(scrollBlock, {
            autoHide: false
        });
    });
}

if (document.querySelector('#table')) {
    const dataTable = new DataTable("#table", {
        searchable: false,
        fixedHeight: false,
        //  fixedColumns: false,
        perPage: 10,
        truncatePager: false,
    })
}
